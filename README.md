# Neil Engine

Example 5D Chess bot using the `5d-chess-js` library, works with built-in chessin5d.net bot api. Aims to be equal or better than the original AI within '5D Chess with Multiverse Time Travel'.

## Copyright

All source code is released under AGPL v3.0 (license can be found under the LICENSE file).

Any addition copyrightable material not covered under AGPL v3.0 is released under CC BY-SA v3.0.
