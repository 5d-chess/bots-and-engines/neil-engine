const Chess = require('5d-chess-js');
const Bot = require('./bot.js');

var chess = new Chess();
var bot1Global = {};
var bot2Global = {};

const keypress = async () => {
  process.stdin.setRawMode(true);
  return new Promise(resolve => process.stdin.once('data', data => {
    const byteArray = [...data];
    if (byteArray.length > 0 && byteArray[0] === 3) {
      console.log('^C');
      process.exit(1);
    }
    process.stdin.setRawMode(false);
    resolve();
  }));
};

(async () => {
  while(!chess.inCheckmate) {
    chess.print();
    var action = {};
    if(chess.player === 'white') {
      action = Bot.botFunc(chess.copy(), null, bot1Global);
    }
    else {
      action = Bot.botFunc(chess.copy(), null, bot2Global);
    }
    console.log(action.moves);
    chess.action(action.moves);
    console.log('Program still running, press any key to continue...');
    await keypress();
  }
  console.log('Checkmate occurred!');
  chess.print();
  console.log(chess.moves('5dpgn'));
})();
