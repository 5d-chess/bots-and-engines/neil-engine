function botFunc(chess, timed, global) {
  /*
    Neil Engine: Simple 5d chess bot utilizing 5d-chess-js library.

    Go to https://gitlab.com/alexbay218/chess-in-5d for more information on how to create your own bot

    For source code visit https://gitlab.com/alexbay218/neil-engine.

    Version: 0.1.2
  */

  //Eval factors (use https://alexbay218.gitlab.io/neil-engine/factorTool/tool.html to create custom eval factor)
  //Default values are based on sunfish.py and modified
  if(typeof global.evalFactors !== 'object') {
    global.evalFactors = {"checkmate":"-Infinity","check":-999999999,"timelineAdvantage":9999999,"inactiveTimelines":-99999,"spatialMove":1,"P":100,"Pa2":-31,"Pa3":-22,"Pa4":-26,"Pa5":-17,"Pa6":7,"Pa7":78,"Pb2":8,"Pb3":9,"Pb4":3,"Pb5":16,"Pb6":29,"Pb7":83,"Pc2":-7,"Pc3":5,"Pc4":10,"Pc5":-2,"Pc6":21,"Pc7":85,"Pd2":19,"Pd3":-11,"Pd4":-2,"Pd5":3,"Pd6":44,"Pd7":73,"Pe2":-36,"Pe3":-10,"Pe4":6,"Pe5":14,"Pe6":40,"Pe7":102,"Pf2":19,"Pf3":-11,"Pf4":-1,"Pf6":31,"Pf7":82,"Pg2":3,"Pg3":3,"Pg5":15,"Pg6":44,"Pg7":85,"Ph2":-31,"Ph3":-19,"Ph4":-23,"Ph5":-13,"Ph6":7,"Ph7":90,"B":320,"Ba1":-7,"Ba2":19,"Ba3":14,"Ba4":13,"Ba5":25,"Ba6":-9,"Ba7":-11,"Ba8":-59,"Bb1":2,"Bb2":20,"Bb3":25,"Bb4":10,"Bb5":17,"Bb6":39,"Bb7":20,"Bb8":-78,"Bc1":-15,"Bc2":11,"Bc3":24,"Bc4":17,"Bc5":20,"Bc6":-32,"Bc7":35,"Bc8":-82,"Bd1":-12,"Bd2":6,"Bd3":15,"Bd4":23,"Bd5":34,"Bd6":41,"Bd7":-42,"Bd8":-76,"Be1":-14,"Be2":7,"Be3":8,"Be4":17,"Be5":26,"Be6":52,"Be7":-39,"Be8":-23,"Bf1":-15,"Bf2":6,"Bf3":25,"Bf4":16,"Bf5":25,"Bf6":-10,"Bf7":31,"Bf8":-107,"Bg1":-10,"Bg2":20,"Bg3":20,"Bg4":0,"Bg5":15,"Bg6":28,"Bg7":2,"Bg8":-37,"Bh1":-10,"Bh2":16,"Bh3":15,"Bh4":7,"Bh5":10,"Bh6":-14,"Bh7":-22,"Bh8":-50,"N":280,"Na1":-74,"Na2":-23,"Na3":-18,"Na4":-1,"Na5":24,"Na6":10,"Na7":-3,"Na8":-66,"Nb1":-23,"Nb2":-15,"Nb3":10,"Nb4":5,"Nb5":24,"Nb6":67,"Nb7":-6,"Nb8":-53,"Nc1":-26,"Nc2":2,"Nc3":13,"Nc4":31,"Nc5":45,"Nc6":1,"Nc7":100,"Nc8":-75,"Nd1":-24,"Nd2":0,"Nd3":22,"Nd4":21,"Nd5":37,"Nd6":74,"Nd7":-36,"Nd8":-75,"Ne1":-19,"Ne2":2,"Ne3":18,"Ne4":22,"Ne5":33,"Ne6":73,"Ne7":4,"Ne8":-10,"Nf1":-35,"Nf2":0,"Nf3":15,"Nf4":35,"Nf5":41,"Nf6":27,"Nf7":62,"Nf8":-55,"Ng1":-22,"Ng2":-23,"Ng3":11,"Ng4":2,"Ng5":25,"Ng6":62,"Ng7":-4,"Ng8":-58,"Nh1":-69,"Nh2":-20,"Nh3":-14,"Nh4":0,"Nh5":17,"Nh6":-2,"Nh7":-14,"Nh8":-70,"R":479,"Ra1":-30,"Ra2":-53,"Ra3":-42,"Ra4":-28,"Ra5":0,"Ra6":19,"Ra7":55,"Ra8":35,"Rb1":-24,"Rb2":-38,"Rb3":-28,"Rb4":-35,"Rb5":5,"Rb6":35,"Rb7":29,"Rb8":29,"Rc1":-18,"Rc2":-31,"Rc3":-42,"Rc4":-16,"Rc5":16,"Rc6":28,"Rc7":56,"Rc8":33,"Rd1":5,"Rd2":-26,"Rd3":-25,"Rd4":-21,"Rd5":13,"Rd6":33,"Rd7":67,"Rd8":4,"Re1":-2,"Re2":-29,"Re3":-25,"Re4":-13,"Re5":18,"Re6":45,"Re7":55,"Re8":37,"Rf1":-18,"Rf2":-43,"Rf3":-35,"Rf4":-29,"Rf5":-4,"Rf6":27,"Rf7":62,"Rf8":33,"Rg1":-31,"Rg2":-44,"Rg3":-26,"Rg4":-46,"Rg5":-9,"Rg6":25,"Rg7":34,"Rg8":56,"Rh1":-32,"Rh2":-53,"Rh3":-46,"Rh4":-30,"Rh5":-6,"Rh6":15,"Rh7":60,"Rh8":50,"S":929,"Sa1":-39,"Sa2":-36,"Sa3":-30,"Sa4":-14,"Sa5":1,"Sa6":-2,"Sa7":14,"Sa8":6,"Sb1":-30,"Sb2":-18,"Sb3":-6,"Sb4":-15,"Sb5":-16,"Sb6":43,"Sb7":32,"Sb8":1,"Sc1":-31,"Sc2":0,"Sc3":-13,"Sc4":-2,"Sc5":22,"Sc6":32,"Sc7":60,"Sc8":-8,"Sd1":-13,"Sd2":-19,"Sd3":-11,"Sd4":-5,"Sd5":17,"Sd6":60,"Sd7":-10,"Sd8":-104,"Se1":-31,"Se2":-15,"Se3":-16,"Se4":-1,"Se5":25,"Se6":72,"Se7":20,"Se8":69,"Sf1":-36,"Sf2":-15,"Sf3":-11,"Sf4":-10,"Sf5":20,"Sf6":63,"Sf7":76,"Sf8":24,"Sg1":-34,"Sg2":-21,"Sg3":-16,"Sg4":-20,"Sg5":-13,"Sg6":43,"Sg7":57,"Sg8":88,"Sh1":-42,"Sh2":-38,"Sh3":-27,"Sh4":-22,"Sh5":-6,"Sh6":2,"Sh7":24,"Sh8":26,"Q":929,"Qa1":-39,"Qa2":-36,"Qa3":-30,"Qa4":-14,"Qa5":1,"Qa6":-2,"Qa7":14,"Qa8":6,"Qb1":-30,"Qb2":-18,"Qb3":-6,"Qb4":-15,"Qb5":-16,"Qb6":43,"Qb7":32,"Qb8":1,"Qc1":-31,"Qc2":0,"Qc3":-13,"Qc4":-2,"Qc5":22,"Qc6":32,"Qc7":60,"Qc8":-8,"Qd1":-13,"Qd2":-19,"Qd3":-11,"Qd4":-5,"Qd5":17,"Qd6":60,"Qd7":-10,"Qd8":-104,"Qe1":-31,"Qe2":-15,"Qe3":-16,"Qe4":-1,"Qe5":25,"Qe6":72,"Qe7":20,"Qe8":69,"Qf1":-36,"Qf2":-15,"Qf3":-11,"Qf4":-10,"Qf5":20,"Qf6":63,"Qf7":76,"Qf8":24,"Qg1":-34,"Qg2":-21,"Qg3":-16,"Qg4":-20,"Qg5":-13,"Qg6":43,"Qg7":57,"Qg8":88,"Qh1":-42,"Qh2":-38,"Qh3":-27,"Qh4":-22,"Qh5":-6,"Qh6":2,"Qh7":24,"Qh8":26,};
  }

  //Set chess object to skip detection and validation for speedup purposes
  chess.skipDetection = true;

  //Check if in nodejs or browser
  if(typeof global.mode !== 'string') {
    global.mode = typeof process !== 'undefined' ? 'nodejs' : 'browser';
  }

  //Difficulty factor to indicate how hard the engine should play (change it for a different difficulty)
  //This modifies the maxTime param by multiplication
  if(typeof global.difficulty !== 'number'){
    global.difficulty = 1;
  }

  //Set default maximum amount of time the engine can use to 30 seconds
  var maxTime = 30000;

  if(timed) {
    //If playing a timed game, engine aims to use 1/40 of the time allotted (with 75% of all increment time as well).
    //This assumes that games played by the engine are going to be 80 actions long (or less).
    maxTime = timed.startingDuration * (1/40) + timed.perActionFlatIncrement * (0.75);
    if(timed.perActionTimelineIncrement > 0) {
      maxTime += chess.board.timelines.filter(t => t.present).length * timed.perActionTimelineIncrement * 0.75;
    }
    maxTime = maxTime * 1000;
  }
  
  //Set amount of overtime that can be used to complete action layer processing (25% of maxTime)
  var overTime = maxTime * 0.25;

  //Difficulty modification
  maxTime *= global.difficulty;
  console.log(`Max Time: ${maxTime}`);

  //Set default maximum amount of ram that can be used (80% of absolute max)
  //If performance.memory is not available, use actionTree length instead
  var maxMemory = 1000000;
  if(typeof performance !== 'undefined' && typeof performance.memory !== 'undefined') {
    maxMemory = Math.floor(performance.memory.jsHeapSizeLimit * (0.80));
    console.log(`Max Memory (MB): ${Math.ceil(maxMemory / 1024 / 1024)}`);
  }
  else if(global.mode === 'nodejs') {
    if(typeof global.v8 === 'undefined') {
      try {
        global.v8 = require('v8');
      }
      catch(err) {
        global.v8 = {
          getHeapStatistics: () => {
            return { total_available_size: 2048 * 1024 * 1024}
          }
        }
      }
    }
    maxMemory = Math.floor(global.v8.getHeapStatistics().total_available_size * (0.80));
    console.log(`Max Memory (MB): ${Math.ceil(maxMemory / 1024 / 1024)}`);
  }
  else {
    console.log(`Max Action Tree Length: ${maxMemory}`);
  }

  //Global definitions
  if(typeof global.actionGen !== 'function') {
    //Function used to generate actions
    global.actionGen = (chess, timeLimit) => {
      var tmpChess = chess.copy();
      //Results array (filled with actions)
      var res = [];
      var inCheck = tmpChess.inCheck;

      //Get all possible moves
      var possibleMoves = chess.moves('object', true, !inCheck, !inCheck);
      //Get present timelines
      var presentTimelines = chess.board.timelines.filter(t => t.present).map(t => t.timeline);

      //Filter out single move actions and non-spatial moves
      //Single move actions will be included in results
      for(var i = 0;i < possibleMoves.length;i++) {
        var currMove = possibleMoves[i];
        try {
          tmpChess.move(currMove);
          if(tmpChess.submittable() && !tmpChess.inCheck) {
            res.push({
              action: chess.actionNumber,
              player: chess.player,
              moves: [currMove]
            });
            if(Date.now() > timeLimit) {
              return res;
            }
            possibleMoves.splice(i,1);
            i--;
          }
          if(
            currMove.start.timeline !== currMove.end.timeline ||
            currMove.start.turn !== currMove.end.turn
          ) {
            possibleMoves.splice(i,1);
            i--;
          }
          tmpChess.state(chess.state());
        }
        catch(err) {}
      }

      //Sort moves into groups by timeline coordinate in source
      var timelineMoves = {};
      var timelineMovesCursor = {}; //Used to track which index in timelineMoves is being used
      for(var i = 0;i < presentTimelines.length;i++) {
        timelineMoves[presentTimelines[i]] = [];
        timelineMovesCursor[presentTimelines[i]] = 0;
      }
      for(var i = 0;i < possibleMoves.length;i++) {
        if(presentTimelines.includes(possibleMoves[i].start.timeline)) {
          timelineMoves[possibleMoves[i].start.timeline].push(possibleMoves[i]);
        }
      }

      //Build combination (move order here does not matter since all moves are non-spatial)
      var done = false;
      tmpChess.state(chess.state());
      while(!done) {
        var currAction = [];
        var keys = Object.keys(timelineMovesCursor);
        for(var i = 0;i < keys.length;i++) {
          if(timelineMovesCursor[keys[i]] < timelineMoves[keys[i]].length) {
            currAction.push(timelineMoves[keys[i]][timelineMovesCursor[keys[i]]]);
          }
        }
        if(currAction.length > 0) {
          try {
            for(var j = 0;j < currAction.moves.length;j++) {
              tmpChess.move(currAction.moves[j]);
            }
            if(tmpChess.submittable() && !tmpChess.inCheck) {
              res.push({
                action: tmpChess.actionNumber,
                player: tmpChess.player,
                moves: currAction
              });
              if(Date.now() > timeLimit) {
                return res;
              }
            }
          }
          catch(err) {}
          tmpChess.state(chess.state());
        }

        var doneInc = false;
        for(var i = 0;!doneInc && i < keys.length;i++) {
          timelineMovesCursor[keys[i]]++;
          if(timelineMovesCursor[keys[i]] < timelineMoves[keys[i]].length) {
            doneInc = true;
          }
          else {
            timelineMovesCursor[keys[i]] = 0;
            if(i + 1 >= keys.length) {
              done = true;
            }
          }
        }
      }
      return res;
    };
  }
  if(typeof global.roughCheckmate !== 'function') {
    //Function used for super rough and fast checkmate detection (does not need to be accurate, since this is for board eval purposes)
    //Considers softmate as checkmate
    global.roughCheckmate = (chess, startTime = Date.now(), maxTime = 1000, checks = -1) => {
      var tmpChess = chess.copy();
      var moves = tmpChess.moves('object', true, true, true);
      if(checks < 0) { checks = tmpChess.checks('object').length; }
      if(checks <= 0) { return false; }
      if((Date.now() - startTime) > maxTime) { return true; }
      for(var i = 0;i < moves.length;i++) {
        tmpChess.move(moves[i]);
        var newChecks = tmpChess.checks('object').length;
        var solvedACheck = newChecks < checks;
        if(solvedACheck) {
          if(newChecks <= 0) {
            return false;
          }
          if(!global.roughCheckmate(tmpChess, startTime, maxTime, newChecks)) {
            return false;
          }
        }
        try {
          tmpChess.undo();
        }
        catch(err) {
          tmpChess.state(chess.state());
        }
      }
      return true;
    };
  }
  if(typeof global.evalFactorProcessing !== 'function') {
    //Function for converting string into numbers for evalFactors
    global.evalFactorProcessing = (factor, player) => {
      var res = {};
      var keys = Object.keys(factor);
      for(var i = 0;i < keys.length;i++) {
        var key = keys[i];
        //Reverse ranks if black
        var match = key.match(/^([A-Z][a-z])(\d)/);
        if(match !== null && player === 'black') {
          key = match[1] + (9 - Number(match[2]));
        }
        var value = Number(factor[keys[i]]);
        if(!Number.isNaN(value)) {
          res[keys[i]] = value;
        }
      }
      return res;
    }
  }
  if(typeof global.boardEval !== 'function') {
    //Function for giving a number based on how good a board is for the current player
    //ExposedKingSquare is currently not implemented
    global.boardEval = (chess, player) => {
      var factors = global.evalFactorProcessing(global.evalFactors, player);
      var value = 0;
      
      //Check for check and checkmate if enabled
      if(factors.check || factors.checkmate) {
        var inCheck = chess.inCheck;
        if(inCheck) {
          if(factors.checkmate) {
            var inCheckmate = global.roughCheckmate(chess.copy());
            if(inCheckmate) {
              value += chess.player === player ? factors.checkmate : -factors.checkmate;
              if(value === Number.NEGATIVE_INFINITY || value === Number.POSITIVE_INFINITY) { return value; }
            }
          }
          if(factors.check) {
            var checks = chess.checks('object').length;
            value += chess.player === player ? factors.check * checks : -factors.check * checks;
            if(value === Number.NEGATIVE_INFINITY || value === Number.POSITIVE_INFINITY) { return value; }
          }
        }
      }

      //Evaluate spatial moves
      if(factors.spatialMove) {
        var tmp = factors.spatialMove * chess.moves('object', true, true, true).length;
        value += chess.player === player ? tmp : -tmp;
        if(value === Number.NEGATIVE_INFINITY || value === Number.POSITIVE_INFINITY) { return value; }
      }

      var board = chess.board;
      var maxTimeline = 0;
      var minTimeline = 0;
      //Evaluate board based off of piece values (only looking at pieces in present boards)
      for(var l = 0;l < board.timelines.length;l++) {
        var currTimeline = board.timelines[l];
        if(currTimeline.active) {
          //Record max and min timelines
          if(maxTimeline < currTimeline.timeline) {
            maxTimeline = currTimeline.timeline;
          }
          if(minTimeline > currTimeline.timeline) {
            minTimeline = currTimeline.timeline;
          }
          var maxTurn = 0;
          var maxTurnPlayer = 'white';
          var maxTurnIndex = 0;
          for(var t = 0;t < currTimeline.turns.length;t++) {
            if(
              currTimeline.turns[t].turn > maxTurn ||
              (currTimeline.turns[t].turn === maxTurn && currTimeline.turns[t].player === 'black' && maxTurnPlayer)
            ) {
              maxTurn = currTimeline.turns[t].turn;
              maxTurnPlayer = currTimeline.turns[t].player;
              maxTurnIndex = t;
            }
          }
          var currTurn = currTimeline.turns[maxTurnIndex];
          for(var p = 0;p < currTurn.pieces.length;p++) {
            var currPiece = currTurn.pieces[p];
            if(currPiece.piece === '' || currPiece.piece === 'P') {
              if(factors['P']) {
                value += currPiece.player === player ? factors['P'] : -factors['P'];
                if(value === Number.NEGATIVE_INFINITY || value === Number.POSITIVE_INFINITY) { return value; }
              }
              if(factors[`P${currPiece.position.coordinate}`]) {
                value += currPiece.player === player ? factors[`P${currPiece.position.coordinate}`] : factors[`P${currPiece.position.coordinate}`];
                if(value === Number.NEGATIVE_INFINITY || value === Number.POSITIVE_INFINITY) { return value; }
              }
            }
            else {
              if(factors[currPiece.piece]) {
                value += currPiece.player === player ? factors[currPiece.piece] : -factors[currPiece.piece];
                if(value === Number.NEGATIVE_INFINITY || value === Number.POSITIVE_INFINITY) { return value; }
              }
              if(factors[`${currPiece.piece}${currPiece.position.coordinate}`]) {
                value += currPiece.player === player ? factors[`${currPiece.piece}${currPiece.position.coordinate}`] : factors[`${currPiece.piece}${currPiece.position.coordinate}`];
                if(value === Number.NEGATIVE_INFINITY || value === Number.POSITIVE_INFINITY) { return value; }
              }
            }
          }
        }
        else {
          //Evaulate inactive timelines
          if(currTimeline.timeline > 0) {
            if(factors.inactiveTimelines) {
              value += player === 'white' ? factors.inactiveTimelines : -factors.inactiveTimelines;
              if(value === Number.NEGATIVE_INFINITY || value === Number.POSITIVE_INFINITY) { return value; }
            }
          }
          if(currTimeline.timeline < 0) {
            if(factors.inactiveTimelines) {
              value += player !== 'white' ? factors.inactiveTimelines : -factors.inactiveTimelines;
              if(value === Number.NEGATIVE_INFINITY || value === Number.POSITIVE_INFINITY) { return value; }
            }
          }
        }
      }
      //Evaluate timeline advantage
      if(factors.timelineAdvantage) {
        if(player !== 'white') {
          value += factors.timelineAdvantage * (minTimeline + maxTimeline);
        }
        else {
          value -= factors.timelineAdvantage * (minTimeline + maxTimeline);
        }
      }
      return value;
    };
  }
  if(typeof global.alphaBeta !== 'function') {
    //Function for deciding action using alphaBeta
    global.alphaBeta = (chess, actionTree, indexTree) => {
      var recurse = (nodeIndex, alpha, beta, maxPlayer) => {
        var node = actionTree[nodeIndex];
        var children = indexTree[nodeIndex];
        if(children.length <= 0) {
          return node.value;
        }
        if(maxPlayer) {
          var value = Number.NEGATIVE_INFINITY;
          var stop = false;
          for(var i = 0;!stop && i < children.length;i++) {
            var ab = recurse(children[i], alpha, beta, !maxPlayer);
            value = Math.max(value, ab);
            alpha = Math.max(value, alpha);
            if(alpha >= beta) {
              stop = true;
            }
          }
          return value;
        }
        else {
          var value = Number.POSITIVE_INFINITY;
          var stop = false;
          for(var i = 0;!stop && i < children.length;i++) {
            var ab = recurse(children[i], alpha, beta, !maxPlayer);
            value = Math.min(value, ab);
            beta = Math.min(value, beta);
            if(alpha >= beta) {
              stop = true;
            }
          }
          return value;
        }
      }
      var rootChildren = indexTree[0];
      var maxValue = Number.NEGATIVE_INFINITY;
      var maxIndex = -1;
      var tmpChess = chess.copy();
      for(var i = 0;i < rootChildren.length;i++) {
        var value = recurse(rootChildren[i], Number.NEGATIVE_INFINITY, Number.POSITIVE_INFINITY, false);
        if(value > maxValue) {
          try {
            var action = actionTree[rootChildren[i]].action;
            for(var j = 0;j < action.moves.length;j++) {
              tmpChess.move(action.moves[j]);
            }
            if(!tmpChess.inCheck) {
              maxValue = value;
              maxIndex = i;
            }
          }
          catch(err) {
            console.error(err)
          }
          tmpChess.state(chess.state());
        }
      }
      if(maxIndex === -1) {
        console.log('No valid action found, forfeiting!');
        throw 'Forfeit!';
      }
      return actionTree[rootChildren[maxIndex]].action;
    };
  }

  //BFS action tree generation
  var tmpChess = chess.copy();
  var tmpChess2 = chess.copy();
  var startTime = Date.now();
  var actionTree = [{
    action: null,
    parent: null,
    value: global.boardEval(tmpChess, chess.player)
  }];
  console.log(`Current board value: ${actionTree[0].value}`);
  var actionTreeIndex = 0;
  var prevActionDepth = 0;
  var currActionDepth = 0;
  var currActionDepthLength = 0;
  var currActionDepthLeft = 0;
  var totalTime = (Date.now() - startTime);
  var done = false;
  while(!done) {
    //Check if over time
    totalTime = (Date.now() - startTime);
    done = totalTime > maxTime;

    //Set tmpChess to reflect current node starting point
    tmpChess.state(chess.state());
    var currNode = actionTree[actionTreeIndex];
    if(currNode.parent !== null) {
      var actionHistory = [currNode.action];
      var parentIndex = currNode.parent;
      while(parentIndex !== null && actionTree[parentIndex].action !== null) {
        actionHistory.push(actionTree[parentIndex].action);
        parentIndex = actionTree[parentIndex].parent;
      }
      for(var i = actionHistory.length - 1;i >= 0;i--) {
        if(actionHistory[i] !== null) {
          tmpChess.action(actionHistory[i]);
        }
      }
      currActionDepth = actionHistory.length;
    }

    //Calculate current action depth layer stats
    if(prevActionDepth < currActionDepth) {
      currActionDepthLength = actionTree.length;
      prevActionDepth = currActionDepth;
    }
    currActionDepthLeft = currActionDepthLength - actionTreeIndex;

    //If over time, check if close to completing current layer
    if(done) {
      var actionProcessRate = (actionTreeIndex + 1) / totalTime;
      if(totalTime + (currActionDepthLeft * actionProcessRate) <= overTime + maxTime) {
        done = false;
      }
    }

    //If over memory limit, force done anyways
    if(typeof performance !== 'undefined' && typeof performance.memory !== 'undefined' && performance.memory.usedJSHeapSize > maxMemory) {
      console.log(`Processing halted due to memory limit (mb): ${performance.memory.usedJSHeapSize / 1024 / 1024}`);
      done = true;
    }
    else if(global.mode === 'nodejs' && global.v8 && process.memoryUsage().heapUsed > maxMemory) {
      console.log(`Processing halted due to memory limit (mb): ${process.memoryUsage().heapUsed / 1024 / 1024}`);
      done = true;
    }
    else if(typeof performance === 'undefined' && actionTree.length > maxMemory) {
      console.log(`Processing halted due to array tree limit: ${actionTree.length}`);
      done = true;
    }

    //Skip if checkmate (don't if action depth is only 1 or lower)
    if((currNode.value !== Number.NEGATIVE_INFINITY && currNode.value !== Number.POSITIVE_INFINITY) || currActionDepth <= 1) {
      //Eval and push child nodes from current node (sort by value [highest first])
      var actions = global.actionGen(tmpChess, startTime + maxTime + overTime);
      var tmpTree = [];
      for(var i = 0;i < actions.length;i++) {
        tmpChess2.state(tmpChess.state());
        tmpChess2.action(actions[i]);
        var value = global.boardEval(tmpChess2, chess.player);
        tmpTree.push({
          action: actions[i],
          parent: actionTreeIndex,
          value: value
        });
      }
      if(chess.player !== tmpChess.player) {
        tmpTree.sort((a, b) => b.value - a.value);
      }
      else {
        tmpTree.sort((a, b) => a.value - b.value);
      }
      for(var i = 0;i < tmpTree.length;i++) {
        actionTree.push(tmpTree[i]);
      }
      tmpTree = null;
    }
    actionTreeIndex++;
    if(actionTreeIndex >= actionTree.length) {
      done = true;
    }
  }
  console.log(`Max action tree depth: ${currActionDepth + 1}`);
  console.log(`Actions processed: ${actionTree.length}`);
  console.log(`Actions left in current depth: ${currActionDepthLeft}`);
  console.log(`Action rate (actions per sec): ${(actionTree.length) / (totalTime / 1000)}`);
  console.log(`Total time (secs): ${(totalTime/1000)}`);

  //Create tree based on action tree indices (subarrays are children of node with the same index in actionTree)
  var indexTree = [];
  for(var i = 0;i < actionTree.length;i++) {
    indexTree.push([]);
    if(actionTree[i].parent !== null) {
      indexTree[actionTree[i].parent].push(i);
    }
  }
  return global.alphaBeta(chess, actionTree, indexTree);
}

//Export if used in nodejs
if(module.exports) { exports.botFunc = botFunc; }
