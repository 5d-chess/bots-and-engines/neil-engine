const exportFunc = () => {
  var res = {};
  
  var value = document.getElementById(`checkmate`).value;
  if(value !== '') {
    res[`checkmate`] = Number(value);
    if(value.includes('Infinity')) {
      res[`checkmate`] = value;
    }
  }
  value = document.getElementById(`check`).value;
  if(value !== '') {
    res[`check`] = Number(value);
    if(value.includes('Infinity')) {
      res[`check`] = value;
    }
  }
  value = document.getElementById(`timelineAdvantage`).value;
  if(value !== '') {
    res[`timelineAdvantage`] = Number(value);
    if(value.includes('Infinity')) {
      res[`timelineAdvantage`] = value;
    }
  }
  value = document.getElementById(`inactiveTimelines`).value;
  if(value !== '') {
    res[`inactiveTimelines`] = Number(value);
    if(value.includes('Infinity')) {
      res[`inactiveTimelines`] = value;
    }
  }
  value = document.getElementById(`spatialMove`).value;
  if(value !== '') {
    res[`spatialMove`] = Number(value);
    if(value.includes('Infinity')) {
      res[`spatialMove`] = value;
    }
  }
  ['P','B','N','R','S','Q','K'].forEach((piece) => {
    value = document.getElementById(`base_${piece}`).value;
    if(value !== '') {
      res[`${piece}`] = Number(value);
      if(value.includes('Infinity')) {
        res[`${piece}`] = value;
      }
    }
    ['a','b','c','d','e','f','g', 'h'].forEach((file) => {
      ['1','2','3','4','5','6','7','8'].forEach((rank) => {
        value = document.getElementById(`table_${piece}_${file}${rank}`).value;
        if(value !== '') {
          res[`${piece}${file}${rank}`] = Number(value);
          if(value.includes('Infinity')) {
            res[`${piece}${file}${rank}`] = value;
          }
        }
      });
    });
  });
  document.getElementById('export').value = JSON.stringify(res);
}

document.getElementById('export_button').onclick = exportFunc;
