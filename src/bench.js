const Chess = require('5d-chess-js');
const Bot = require('./bot.js');

var chess = new Chess();
var bot1Global = { difficulty: 1 };
var bot2Global = { difficulty: 1 };

console.time('Total Bot Time');
for(var i = 0;!chess.inCheckmate && i < 26;i++) {
  var action = {};

  chess.print();
  if(chess.player === 'white') {
    action = Bot.botFunc(chess.copy(), null, bot1Global);
  }
  else {
    action = Bot.botFunc(chess.copy(), null, bot2Global);
  }
  try {
    chess.action(action);
    console.log(chess.export('5dpgn'));
  }
  catch(err) {
    console.log(JSON.stringify(action, null, 2));
    console.log(chess.moves('5dpgn', false, false));
    console.log(chess.export('5dpgn'));
    throw err;
  }
}
console.timeEnd('Total Bot Time');
console.log(chess.export('5dpgn'));
